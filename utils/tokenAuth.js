const jwt = require('jsonwebtoken');

const tokenCheck = (secreatKey) => async function (req, res, next) {
    try {
        if (!req.headers.authorization) {
            return res.status(400).json({ message: 'Auth header missing' });
        }
        const token = (req.headers.authorization).split(' ')[1];
        if (!token) {
            return res.status(400).json({ message: 'Token missing' });
        }
        const isUserCorrect = jwt.verify(token, secreatKey);
        if (!isUserCorrect) {
            return res.status(400).json({ message: 'Incorrect User' });
        }
        return next();
    } catch (error) {
        console.log(error.stack);
        if (error.message === 'jwt expired') {
            return res.status(400).json({ message: 'Session Expired' });
        }
        return res.status(400).json({ message: error.message });
    }
}

module.exports = {
    tokenCheck,
}