const { Model } = require('objection');
class Users extends Model {
    static get tableName() {
        return 'users';
    }
    $beforeInsert() {
        this.created_at = new Date();
    }
    $beforeUpdate() {
        this.updated_at = new Date();
    }
    // static relationMappings() {
    //     return { 

    //     }
    // }
}

module.exports = Users;
