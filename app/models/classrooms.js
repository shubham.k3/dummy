const { Model } = require('objection');
class Classrooms extends Model {
    static get tableName() {
        return 'classrooms';
    }
    $beforeInsert() {
        this.created_at = new Date();
    }
    $beforeUpdate() {
        this.updated_at = new Date();
    }
    // static relationMappings() {
    //     return { 

    //     }
    // }
}

module.exports = Classrooms;
