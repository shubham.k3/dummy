const baseString = '/users';
const express = require('express');
const router = express.Router();

// Controllers
const Users = require('../controllers/users');

router.get(`${baseString}/`, Users.getUsers);
router.post(`${baseString}/`, Users.addUser);
router.post(`${baseString}/signup`, Users.signup);
router.post(`${baseString}/login`, Users.login);
router.put(`${baseString}/:user_id`, Users.modifyUser);
router.get(`${baseString}/:user_id`, Users.getUser);
router.delete(`${baseString}/:user_id`, Users.removeUser);

module.exports = router;