const Users = require('./users');
const Classrooms = require('./classrooms');

module.exports = [
    Users,
    Classrooms,
]