const baseString = '/classrooms';
const express = require('express');
const router = express.Router();
const secreatKey = '123456789';

// Middlewares
const Auth = require('../../utils/tokenAuth');

// Controllers
const Classrooms = require('../controllers/classrooms');

router.post(`${baseString}/`, [Auth.tokenCheck(secreatKey)], Classrooms.addClassRoom);
router.post(`${baseString}/uploadSingleFile`, Classrooms.uploadFile);
router.post(`${baseString}/uploadMultipleFile`, Classrooms.uploadMultipleFile);

module.exports = router;
