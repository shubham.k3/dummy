const Joi = require('joi');

const addUser = Joi.object({
    name: Joi.string().required().messages({ 'any.required': 'Please enter name' }),
    email: Joi.string().required().messages({ 'any.required': 'Please enter email' }),
    password: Joi.string().required().messages({ 'any.required': 'Please enter password' }),
    gender: Joi.string().required().messages({ 'any.required': 'Please enter gender' }),
});

const modifyUser = Joi.object({
    name: Joi.string(),
    email: Joi.string(),
    password: Joi.string(),
    gender: Joi.string(),
});

const login = Joi.object({
    email: Joi.string().required().messages({ 'any.required': 'Please enter email' }),
    password: Joi.string().required().messages({ 'any.required': 'Please enter password' }),
});

module.exports = {
    addUser,
    modifyUser,
    login,
}