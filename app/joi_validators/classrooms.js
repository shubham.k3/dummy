const Joi = require('joi');

const addClassRoom = Joi.object({
    room_no: Joi.number().required().messages({ 'any.required': 'Please enter room no' }),
    room_section: Joi.string().required().messages({ 'any.required': 'Please enter section' }),
});

module.exports = {
    addClassRoom,
}