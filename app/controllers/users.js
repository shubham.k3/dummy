const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const secreatKey = '123456789';

// Models
const Users = require('../models/users');

// Validators
const validator = require('../joi_validators/users');

const getUsers = async function (req, res) {
    try{
        let pageNo = req.query.page ? req.query.page : 1;
        let limit = req.query.limit ? req.query.limit : 10;
        let offset = req.query.offset ? req.query.offset : limit * (pageNo - 1);

        const allUsers = await Users.query().select('*').limit(limit).offset(offset);
        const responseData = {
            users: allUsers,
        }
        return res.status(200).json({ data: responseData, message: 'data found successfully' });
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
};

const getUser = async function (req, res) {
    try{
        let params = req.params;
        if (!params.user_id) {
            return res.status(400).json({ error: 'Please pass user id' });
        }
        const aUser = await Users.query().select('*').where({ user_id: params.user_id });
        if (aUser.length === 0) {
            return res.status(400).json({ error: 'Sorry! user not found' });
        } else {
            const responseData = {
                users: aUser,
            }
            return res.status(200).json({ data: responseData, message: 'data found successfully' });
        }
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
};

const addUser = async function (req, res) {
    try{
        let data = req.body;
        const validateData = await validator.addUser.validate(data);
        if (validateData && validateData.error && validateData.error.details[0]) {
            return res.status(400).json({ error: validateData.error.details[0].message });
        }

        const salt = await bcrypt.genSalt(parseInt(10, 10));
        data.password = await bcrypt.hash(data.password, salt);
        const getUser = await Users.query().insert(data);
        const responseData = {
            user: getUser,
        }
        return res.status(200).json({ data: responseData, message: 'data found successfully' });
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
};

const modifyUser = async function (req, res) {
    try{
        let data = req.body;
        let params = req.params;
        if (!params.user_id) {
        return res.status(400).json({ error: 'Please pass user id' });
        }

        const validateData = await validator.modifyUser.validate(data);
        if (validateData && validateData.error && validateData.error.details[0]) {
            return res.status(400).json({ error: validateData.error.details[0].message });
        }

        const getUser = await Users.query().update(data).where({ user_id: params.user_id });
        const responseData = {
            user: getUser,
        }
        return res.status(200).json({ data: responseData, message: 'data updated successfully' });
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
};

const removeUser = async function (req, res) {
    try{
        let params = req.params;
        if (!params.user_id) {
        return res.status(400).json({ error: 'Please pass user id' });
        }
        const aUser = await Users.query().delete().where({ user_id: params.user_id });
        const responseData = {
            users: aUser,
        }
        return res.status(200).json({ data: responseData, message: 'data deleted successfully' });
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
};

const removeAllUsers = async function (req, res) {
    try{
        const aUser = await Users.query().delete();
        const responseData = {
            users: aUser,
        }
        return res.status(200).json({ data: responseData, message: 'all data deleted successfully' });
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
};

// ------------------------- Signup/Login ------------------------------------ //
const signup = async function (req, res) {
    try{
        const expirationTime = 3600 * 24 * 7; // 7 Day

        let data = req.body;
        const validateData = await validator.addUser.validate(data);
        if (validateData && validateData.error && validateData.error.details[0]) {
            return res.status(400).json({ error: validateData.error.details[0].message });
        }

        const salt = await bcrypt.genSalt(10);
        data.password = await bcrypt.hash(data.password, salt);        
        // const verifyPassword = await bcrypt.compare('shubham', data.password);
        const getUser = await Users.query().insert(data);

        let token = jwt.sign({ user_id: getUser.$id, email: getUser.email }, secreatKey, { expiresIn: expirationTime });
        // var decoded = jwt.verify(token, secreatKey); // 'wrong-secret');

        res.setHeader('Content-Type', 'application/json');
        res.setHeader('Authorization', `Bearer ${token}`);
        res.setHeader('Access-Control-Expose-Headers', [
            'Authorization',
            'x-amzn-Remapped-authorization',
        ]);

        const responseData = {
            users: getUser,
            token,
            // decoded,
            salt,
            // encriptedPassword,
            // verifyPassword,
        }
        return res.status(200).json({ data: responseData, message: 'data found successfully' });
    } catch (error) {
        console.log(error.stack);
        return res.status(400).json({ error: error.message });
    }
};

const login = async function (req, res) {
    try {
        let data = req.body;
        const validateData = await validator.login.validate(data);
        if (validateData && validateData.error && validateData.error.details[0]) {
            return res.status(400).json({ error: validateData.error.details[0].message });
        }

        const userInfo = await Users.query().where({ email: data.email }).select('user_id', 'password', 'email').first();
        if (!userInfo) {
            return res.status(400).json({ message: 'Sorry email not found' });
        }
        const isPasswordMatched = await bcrypt.compare(data.password, userInfo.password);
        if (!isPasswordMatched) {
            return res.status(400).json({ message: 'Sorry Password not matched' });
        }

        let token = jwt.sign({ email: userInfo.email, user_id: userInfo.user_id }, secreatKey, { expiresIn: 10 });
        res.setHeader('Content-Type', 'application/json');
        res.setHeader('Authorization', `Bearer ${token}`);
        res.setHeader('Access-Control-Expose-Headers', [
            'Authorization',
            'x-amzn-Remapped-authorization',
        ]);

        const responseData = {
            user: { user_id: userInfo.user_id, email: userInfo.email },
        }
        return res.status(200).json({ message: 'logged in successfully', data: responseData });
    } catch (error) {
        console.log(error.stack);
        return res.status(400).json({ message: error.message });
    }
};

module.exports = {
    addUser,
    getUser,
    getUsers,
    modifyUser,
    removeUser,
    removeAllUsers,
    signup,
    login,
}
