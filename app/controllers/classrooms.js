const util = require('util');
const express = require('express');
const path = require('path');
const multer = require('multer');
const app = express();

// Models
const Classrooms = require('../models/classrooms');

// Validators
const validator = require('../joi_validators/classrooms');

const addClassRoom = async function (req, res) {
    try{
        let data = req.body;
        const validateData = await validator.addClassRoom.validate(data);
        if (validateData && validateData.error && validateData.error.details[0]) {
            return res.status(400).json({ error: validateData.error.details[0].message });
        }
        const classRooms = await Classrooms.query().insert(data);
        const responseData = {
            classrooms: classRooms,
        }
        return res.status(200).json({ data: responseData, message: 'data added successfully' });
    } catch (error) {
        return res.status(400).json({ error: error.message });
    }
};

const upload = util.promisify(multer({
    storage: multer.diskStorage({
        destination: function (req, file, cb) {
            cb(null, 'upload');
        },
        filename: function (req, file, cb) {
            cb(null, file.originalname.split('.')[0] + '_' + Date.now() + '.' + file.originalname.split('.')[1]);
        }
    }),
    limits: { fileSize: 1 * 1000 * 1000 }, // 1MB
    fileFilter: function (req, file, cb) {
        var filetypes = /jpeg|jpg|png/;
        var mimetype = filetypes.test(file.mimetype);
        var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        if (mimetype && extname) {
            return cb(null, true);
        }
        cb(new Error('Error: File upload only supports the following filetypes - .' + filetypes));
    }
}).single('mypic'));

const uploadMultiple = util.promisify(multer({
    storage: multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'upload');
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname.split('.')[0] + '_' + Date.now() + '.' + file.originalname.split('.')[1]);
    }
}),
  limits: { fileSize: 1 * 1000 * 1000 }, // 1MB
    fileFilter: function (req, file, cb) {
        var filetypes = /jpeg|jpg|png/;
        var mimetype = filetypes.test(file.mimetype);
        var extname = filetypes.test(path.extname(file.originalname).toLowerCase());
        if (mimetype && extname) {
            return cb(null, true);
        }
        cb(new Error('Error: File upload only supports the following filetypes - .' + filetypes));
    }
}).array('mypic', 3));

const uploadFile = async (req, res) => {
    try {
        await upload(req, res);
        const rel_path = req.file.path;
        const responseData = {
            classrooms: 'classRooms',
            img_path: rel_path,
        };

        return res.status(200).json({ data: responseData, message: 'data added successfully' });
    } catch (error) {
        console.log('line: ' + error.stack);
        return res.status(400).json({ error: error.message });
    }
};

const uploadMultipleFile = async (req, res) => {
    try {
        await uploadMultiple(req, res);
        const rel_path = req.files;
        const responseData = {
            classrooms: 'classRooms',
            img_path: rel_path,
        };

        return res.status(200).json({ data: responseData, message: 'data added successfully' });
    } catch (error) {
        console.log('line: ' + error.stack);
        return res.status(400).json({ error: error.message });
    }
};

module.exports = {
    addClassRoom,
    uploadFile,
    uploadMultipleFile,
}
