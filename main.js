const express = require('express');
const logger = require('morgan');
const cors = require('cors');
const http = require("http");
const path = require("path");
const knex = require('knex');
const app = express();
const fs = require("fs");
const knexConfig = require('./config/knex');
const knexInstance = knex(knexConfig['development']);
const { Model } = require('objection');
app.use(logger('dev'));
app.use(cors());
app.options('*', cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.set({ 'port': 3002 });
const server = http.createServer(app);

const route = require('./app/routes');
app.use('', route);

Model.knex(knexInstance);
knexInstance.raw('SELECT 1+1 as result')
.then(() => {
    console.log('Database is connected');
})
.catch((error) => {
    console.error('Error connecting to database', error.message);
});

// Email Testing
// const mail = require('./utils/mail');
// mail.sendToSingle('kumarshubham031@gmail.com', 'Sending with SendGrid is Fun', 'welcome.pug', 'value');

app.use(express.static(path.join(__dirname, 'views')));

server.listen(3002, function () {
    console.log('PORT: 3002');
    console.log('server running successfuly');
});