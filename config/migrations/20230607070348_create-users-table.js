exports.up = function(knex) {
    return knex.schema.createTable('users', (table) => {
        table.increments('user_id').unique().notNullable();
        table.string('name').notNullable();
        table.string('email').notNullable();
        table.string('password').notNullable();
        table.string('gender').notNullable();
        table.timestamps(true);
    });
};
exports.down = function(knex) {
    // return knex.schema.dropTable('users');
};
