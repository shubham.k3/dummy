exports.up = function(knex) {
    return knex.schema.createTable('classrooms', (table) => {
        table.increments('room_id').unique().notNullable();
        table.integer('room_no').notNullable();
        table.string('room_section').notNullable();
        table.timestamps(true);
    });
};
exports.down = function(knex) {
    // return knex.schema.dropTable('classrooms');
};
