const development = {
    client: 'mysql2',
    useNullAsDefault: true,
    connection: {
        host: 'localhost',
        database: 'dummy',
        user: 'username',
        password: 'Sst12345@#',
    },
    migrations: {
        directory: './migrations',
    },
    seeds: {
        directory: './seeds',
    }
};
module.exports = {
    development,
};